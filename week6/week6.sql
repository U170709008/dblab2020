# 1. Show the films whose budget is greater than 10 million$ and ranking is less than 6.
SELECT title 
from movies
where budget > 10000000 AND ranking<6;

# 2. Show the action films whose rating is greater than 8.8 and produced after 2009.
SELECT *
from movies
JOIN genres  ON movies.movie_id = genres.movie_id
where genre_name = "Action" AND rating > 8.8 AND year > 2009  ;

# 3. Show the drama films whose duration is more than 150 minutes and oscars is more than 2.
SELECT * 
FROM movies m
JOIN genres g ON m.movie_id = g.movie_id
where genre_name = "Drama" and duration>150 and oscars>2;

# 4. Show the films that Orlando Bloom and Ian McKellen have act together and has more than 2 Oscars.
SELECT title,oscars,S.star_name
FROM movies M
JOIN movie_stars MS ON M.movie_id = MS.movie_id
JOIN stars S ON S.star_id = MS.star_id
where S.star_name = "Orlando Bloom" AND M.movie_id IN (SELECT M.movie_id
FROM movies M
JOIN movie_stars MS ON M.movie_id = MS.movie_id
JOIN stars S ON S.star_id = MS.star_id
where S.star_name = "Ian McKellen") AND oscars > 2;
# 5. Show the Quentin Tarantino films which have more than 500000 votes and produced before 2000.	 

# 6. Show the thriller films whose budget is greater than 25 million$.	 

# 7. Show the drama films whose language is Italian and produced between 1990-2000.	
 
# 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.	 

# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.

# 10.Compute the average budget of the films directed by Peter Jackson.

# 11.Show the Francis Ford Coppola film that has the minimum budget.

# 12.Show the film that has the most vote and has been produced in USA.